package ep2;

public class Jogador{
	private final Tabuleiro tabuleiro;
	private int recursos;
	private int turno;
	
	public static final int ATAQUE_SIMPLES=1;
	public static final int ATAQUE_AREA=3;
	public static final int ATAQUE_LINHA=5;
	public static final int ATAQUE_COLUNA=5;
	public static final int VISAO=2;
	public static final int SUCESSO=17;
	
	public Jogador(int largura, int altura, int recursos){
		this.tabuleiro= new Tabuleiro(largura, altura);
		this.tabuleiro.PosicionarBarcos();
		this.recursos=recursos;
		this.turno=0;
	}
	
	public Jogador(int largura, int altura){
		this.tabuleiro= new Tabuleiro(largura, altura);
		this.tabuleiro.PosicionarBarcos();
		this.recursos=50;
		this.turno=0;
	}
	
	public Jogador(Tabuleiro tabuleiro, int recursos){
		this.tabuleiro=tabuleiro;
		this.recursos=recursos;
		this.turno=0;
	}
	
	public Jogador(Tabuleiro tabuleiro){
		this.tabuleiro=tabuleiro;
		this.recursos=50;
		this.turno=0;
	}
	public static final int SEM_RECURSOS=0;
	public static final int BARCO_ATACADO=1;
	
	public static final int DERROTA=0;
	public static final int VITORIA=1;
	public static final int JOGANDO=2;
	public int Status(){
		int temp;
		for (int i=0; i<this.tabuleiro.getAltura(); i++){
			for(int j=0; j<this.tabuleiro.getLargura(); j++){
				temp=this.tabuleiro.getTabuleiro(i,j);
				if(temp==Tabuleiro.BARCO){
					if(this.recursos==0)
						return DERROTA;
					else
						return JOGANDO; 
				}
			}
		}
		return VITORIA;
	}
	public int AtaqueSimples(int linha, int coluna){
		
		int temp=this.tabuleiro.getTabuleiro(linha,coluna);
		
		if(recursos<ATAQUE_SIMPLES) {
			return SEM_RECURSOS;
		}
		
		if(temp==Tabuleiro.AGUA){
			tabuleiro.setTabuleiro(linha, coluna, Tabuleiro.TIRO_AGUA);
			this.recursos-=ATAQUE_SIMPLES;
			this.turno++;
			return SUCESSO;
			
		}
		
		else if(temp==Tabuleiro.BARCO){
			tabuleiro.setTabuleiro(linha, coluna, Tabuleiro.TIRO_BARCO);
			this.recursos++;
			this.turno++;
			return SUCESSO;
		}
		
		else{
			return BARCO_ATACADO;
		}
	}
	
	private void AtaqueSimples(int linha, int coluna, char arg){
		
		int temp=this.tabuleiro.getTabuleiro(linha,coluna);
		
		if(temp==Tabuleiro.AGUA){
			tabuleiro.setTabuleiro(linha, coluna, Tabuleiro.TIRO_AGUA);
		}
		
		else if(temp==Tabuleiro.BARCO){
                    recursos++;
                    tabuleiro.setTabuleiro(linha, coluna, Tabuleiro.TIRO_BARCO);
		}
	}
	private static final int AREA_ATAQUE=2;
	public static final int AREA_ATACADA=1;
	public static final int CASA_INVALIDA=4;
	
	public int AtaqueArea(int linha, int coluna){
		if(linha+AREA_ATAQUE>this.tabuleiro.getAltura() || coluna+AREA_ATAQUE>this.tabuleiro.getLargura()){
			return CASA_INVALIDA;
		}
		if(recursos<ATAQUE_AREA){
			return SEM_RECURSOS;
		}
		
		
		boolean temp=false;
		
		for(int i=linha; i<linha+AREA_ATAQUE; i++){
			for(int j=coluna; j<coluna+AREA_ATAQUE; j++){
				if(this.tabuleiro.getTabuleiro(i,j)==Tabuleiro.INVALIDO){
					temp=false;
					break;
				}
			    else if(this.tabuleiro.getTabuleiro(i,j)==Tabuleiro.AGUA || this.tabuleiro.getTabuleiro(i,j)==Tabuleiro.BARCO){
			       	temp=true;
			    }
			}
		}
		
		if(temp){
			for(int i=linha; i<linha+AREA_ATAQUE; i++){
				for(int j=coluna; j<coluna+AREA_ATAQUE; j++){
					AtaqueSimples(i,j,'a');
				}
			}
			this.recursos-=ATAQUE_AREA;
			this.turno++;
			return SUCESSO;
		}
		else
			return AREA_ATACADA;
	}
	
	private static final int AREA_VISAO=2;
	
	public static final int TEM_BARCO=1;
	public static final int NAO_TEM_BARCO=2;
	public int Visao(int linha, int coluna){
		if(recursos<VISAO){
			return SEM_RECURSOS;
		}
		this.recursos-=VISAO;
		this.turno++;
		for(int i=linha; i<linha+AREA_VISAO; i++){
			for(int j=coluna; j<coluna+AREA_VISAO; j++){
				if(this.tabuleiro.getTabuleiro(i,j)==Tabuleiro.BARCO){
					return TEM_BARCO;
				}
			}
		}
		return NAO_TEM_BARCO;
	}
	public static final int LINHA_INVALIDA=1;
	
	public int AtaqueColuna(int coluna){
		if(recursos<ATAQUE_LINHA)
			return SEM_RECURSOS;
		int num=tabuleiro.getAltura();

		if(coluna>tabuleiro.getAltura())
			return LINHA_INVALIDA;
		for(int i=0; i<num; i++)
			AtaqueSimples(i, coluna,'a');
		this.recursos-=ATAQUE_LINHA;
		this.turno++;
		return SUCESSO;
	}
	
	public static final int COLUNA_INVALIDA=1;
	public int AtaqueLinha(int linha){
		if(recursos<ATAQUE_COLUNA)
			return SEM_RECURSOS;
		int num=tabuleiro.getLargura();
		if(linha>tabuleiro.getLargura())
			return COLUNA_INVALIDA;
		for(int i=0; i<num; i++){
			AtaqueSimples(linha, i,'a');
		}
		this.recursos-=ATAQUE_COLUNA;
		this.turno++;
		return SUCESSO;
	}
	
	public int getRecursos(){
		return recursos;
	}
	
	public int getTurno(){
		return turno;
	}
}