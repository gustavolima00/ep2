package ep2;

import java.awt.Image;
import java.awt.event.*;
import javax.swing.*;

public final class JogoJanela extends JFrame{
    
        private javax.swing.ButtonGroup botoes;
        private javax.swing.JRadioButton tiroArea;
        private javax.swing.JRadioButton tiroColuna;
        private javax.swing.JRadioButton tiroLinha;
        private javax.swing.JRadioButton tiroSimples;
        private javax.swing.JLabel titulo;
        private javax.swing.JRadioButton visao;
        private java.awt.Label pontos;
            
	JogoJanela(Tabuleiro tabuleiro, int pontosIniciais){

            int nLinhas=tabuleiro.getAltura(), nColunas=tabuleiro.getLargura();  
            Jogador jogador = new Jogador(tabuleiro, pontosIniciais);
            JogoCanvas canvas = new JogoCanvas(tabuleiro);
            CanvasThread thread = new CanvasThread(canvas);
                  
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setLocationRelativeTo(null);
            pontos = new java.awt.Label();
            botoes = new javax.swing.ButtonGroup();
            tiroSimples = new javax.swing.JRadioButton();
            titulo = new javax.swing.JLabel();
            tiroArea = new javax.swing.JRadioButton();
            tiroColuna = new javax.swing.JRadioButton();
            tiroLinha = new javax.swing.JRadioButton();
            visao = new javax.swing.JRadioButton();

            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

            titulo.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
            titulo.setText("Batalha Naval 2.0");
            
            tiroSimples.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
            tiroSimples.setText("Tiro Simples");
            botoes.add(tiroSimples);

            tiroArea.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
            tiroArea.setText("Tiro em Área 2x2");
            botoes.add(tiroArea);

            tiroColuna.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
            tiroColuna.setText("Tiro na Coluna");
            botoes.add(tiroColuna);

            tiroLinha.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
            tiroLinha.setText("Tiro na Linha");
            botoes.add(tiroLinha);

            visao.setFont(new java.awt.Font("MV Boli", 0, 14)); // NOI18N
            visao.setText("Visão 2x2");
            botoes.add(visao);
            
            pontos.setFont(new java.awt.Font("MV Boli", 1, 16)); // NOI18N
            pontos.setForeground(new java.awt.Color(0, 102, 102));
            pontos.setText("Pontos:"+jogador.getRecursos());

            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(visao)
                                .addComponent(tiroLinha)
                                .addComponent(tiroColuna)
                                .addComponent(tiroSimples)
                                .addComponent(tiroArea))
                            .addGap(90, 90, 90)
                            .addComponent(canvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(titulo)
                            .addGap(110, 110, 110)
                            .addComponent(pontos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(pontos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(titulo))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(tiroSimples)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(tiroArea)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(tiroLinha)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(tiroColuna)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(visao)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(17, 17, 17)
                            .addComponent(canvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap())
            );

            pack();
            
            setSize(340+40*nColunas, 120+40*nLinhas);
            
            setVisible(true);
            thread.start(); 
                
            canvas.addMouseListener(new MouseListener() {
                
                @Override
                public void mouseReleased(MouseEvent e) {
                    
                }
                
                @Override
                public void mouseClicked(MouseEvent e){                    
                    int x=e.getX();
                    int y=e.getY();                   
                    int matriz_x=-1, matriz_y=-1;
                    
                    if(x>=JogoCanvas.TAVULEIRO_INICIO_X && x<=JogoCanvas.TAVULEIRO_INICIO_X+tabuleiro.getLargura()*JogoCanvas.TAVULEIRO_TAMANHO_CASA){
                        matriz_x=(x-JogoCanvas.TAVULEIRO_INICIO_X)/JogoCanvas.TAVULEIRO_TAMANHO_CASA;
                    }
                    if(y>=JogoCanvas.TAVULEIRO_INICIO_Y && y<=JogoCanvas.TAVULEIRO_INICIO_Y+tabuleiro.getAltura()*JogoCanvas.TAVULEIRO_TAMANHO_CASA){
                        matriz_y=(y-JogoCanvas.TAVULEIRO_INICIO_Y)/JogoCanvas.TAVULEIRO_TAMANHO_CASA;
                    }
                    
                    //System.out.println("Clique x="+matriz_x+"\ty="+matriz_y);
                    //canvas.setTabuleiro(matriz_x, matriz_y, JogoCanvas.MOUSE_CLIQUE);
                    if(matriz_x>=0 && matriz_y>=0){
                        if(tiroSimples.isSelected()){
                            jogador.AtaqueSimples(matriz_y, matriz_x);
                            canvas.Atirar(matriz_y,matriz_x);
                        }
                        else if(tiroArea.isSelected()){
                            jogador.AtaqueArea(matriz_y, matriz_x);
                            canvas.Atirar(matriz_y,matriz_x);
                            canvas.Atirar(matriz_y+1,matriz_x);
                            canvas.Atirar(matriz_y+1,matriz_x+1);
                            canvas.Atirar(matriz_y,matriz_x+1);
                        }
                        else if(tiroLinha.isSelected()){
                            jogador.AtaqueLinha(matriz_y);
                            for(int i=0; i<nColunas; i++)
                                canvas.Atirar(matriz_y,i);
                        }
                        else if(tiroColuna.isSelected()){
                            jogador.AtaqueColuna(matriz_x);
                            for(int i=0; i<nLinhas; i++)
                                canvas.Atirar(i,matriz_x);
                        }
                        else if(visao.isSelected()){
                            int visao;
                            visao=jogador.Visao(matriz_y, matriz_x);
                            switch(visao){
                                case Jogador.TEM_BARCO:
                                    JOptionPane.showMessageDialog(null, "Possui algum barco nessa região");
                                    break;
                                case Jogador.NAO_TEM_BARCO:
                                    JOptionPane.showMessageDialog(null, "não possuem barcos nessa região");
                                    break;

                            }
                        }
                    }
                    if(jogador.Status()==Jogador.VITORIA){
                    JOptionPane.showMessageDialog(null, "Parabéns Você venceu");
                    System.exit(0);
                    }
                    else if(jogador.Status()==Jogador.DERROTA){
                        JOptionPane.showMessageDialog(null, "Seus pontos acabaram, você perdeu");
                        System.exit(0);
                    }
                    pontos.setText("Pontos:"+jogador.getRecursos());
                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                 
                }

                @Override 
                public void mouseExited(MouseEvent e) {
                }


		});
        }
}