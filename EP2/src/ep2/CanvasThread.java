/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

/**
 *
 * @author Gustavo
 */
public class CanvasThread extends Thread {
    private final JogoCanvas canvas;
    private final boolean running = true; 
    
    public CanvasThread(JogoCanvas canvas) {
	this.canvas = canvas;
    }
    
    @Override
	public void run() {
		while(running) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			canvas.paint(canvas.getGraphics());
		}
	}    
}
