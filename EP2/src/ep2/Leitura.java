package ep2;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Character.getNumericValue;
import java.util.Scanner;

public class Leitura {
    private int largura=0, altura=0;
    private int matrix[][] = null;
    private int n_barcos=0;
    private final int barcos[][]=new int[9][2];
    Leitura(File file)throws IOException{
        String linha;
        Scanner ler;
        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            br.readLine();
            linha=br.readLine();
            ler= new Scanner(linha);
            largura = ler.nextInt();
            altura = ler.nextInt();
            //System.out.println("Altura: "+altura+"\tLargura: "+largura);
            br.readLine();
            br.readLine();
            //System.out.println(linha);
            matrix= new int[altura][largura];
            for(int i=0; i<altura; i++){
                linha=br.readLine();
                for(int j=0; j<largura; j++){
                    matrix[i][j]=getNumericValue(linha.charAt(j));
                    //System.out.print(matrix[i][j]+" ");
                }
                //System.out.println(" ");
            }
            //br.readLine();
            //br.readLine();
            //System.out.println("Ultima linha: "+ linha);
            while(br.ready()){
                linha = br.readLine();
                //ler= new Scanner(linha);
                //barcos[n_barcos][0]=ler.nextInt();
                // barcos[n_barcos][1]=ler.nextInt();
                //n_barcos++;
            }
        }
    }
    int getLargura(){
        return largura;
    }
    int getAltura(){
        return altura;
    }
    int getMatrix(int i, int j){
        if((i<=altura && i>=0) && (j<=largura && j>=0))
            return matrix[i][j]; 
        else
            return -1;
    }
    int getBarco(int i, int j){
        if((i<=n_barcos && i>=0) && (j==1 || j==0))
            return barcos[i][j];
        else 
            return -1;
    }
}
