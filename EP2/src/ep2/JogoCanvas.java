/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ep2;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Gustavo
 */
public class JogoCanvas extends Canvas{
    private int largura; 
    private int altura;
    private int matrix[][];
    private Tabuleiro tabuleiro;
    private Jogador jogador;
    int animationCounterAgua=0;
    int animationCounterFogo=0;
    int animationCounterBanana=0;
    int animationCounterBoom=0;
    int animationCounterFumaca=0;
    public static final int TAVULEIRO_INICIO_X=0;
    public static final int TAVULEIRO_INICIO_Y=0;
    public static final int TAVULEIRO_TAMANHO_CASA=40;
    
    public static final int MOUSE_EM_CIMA=1;
    public static final int MOUSE_CLIQUE=2;
    public static final int MOUSE_SOLTAR=3;
    public static final int SEM_MOUSE=-1;
    private static final Color M_TRED = new Color(255, 0, 0, 150);
    private static final Color M_TGREEN = new Color(0, 255, 0, 150);
    private static final Color M_TBLUE = new Color(0, 0, 255, 150);
    private static final int ATIRANDO=1;
    private static final int NORMAL=0;
    
    JogoCanvas(Tabuleiro tabuleiro){
        this.largura=tabuleiro.getLargura();
        this.altura=tabuleiro.getAltura();
        matrix = new int[altura][largura];
        for(int i=0; i<altura; i++){
            for(int j=0; j<largura; j++){
                matrix[i][j]=NORMAL;
            }
        }
        this.tabuleiro=tabuleiro;
    }
    @Override
    public void paint(Graphics g){
        atualizaAnimacoes();       
        g.setColor(Color.BLACK);
        for(int i=0; i<=altura; i++){
            g.drawLine(TAVULEIRO_INICIO_X, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i, TAVULEIRO_INICIO_X+largura*TAVULEIRO_TAMANHO_CASA, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i);
        }
        for(int i=0; i<=largura; i++){
            g.drawLine(TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*i, TAVULEIRO_INICIO_Y, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*i, TAVULEIRO_INICIO_Y+altura*TAVULEIRO_TAMANHO_CASA);
        }
        ImageIcon agua = new ImageIcon("img/agua_"+String.valueOf(animationCounterAgua)+".png");
        ImageIcon fogo = new ImageIcon("img/fogo_"+String.valueOf(animationCounterFogo)+".png");
        ImageIcon banana = new ImageIcon("img/banana_"+String.valueOf(animationCounterBanana)+".png");
        ImageIcon boom = new ImageIcon("img/boom_"+String.valueOf(animationCounterBoom)+".png");
        ImageIcon fumaca = new ImageIcon("img/fumaca_"+String.valueOf(animationCounterFumaca)+".png");
        final Image img_agua = agua.getImage();
        final Image img_fogo = fogo.getImage();
        final Image img_banana = banana.getImage();
        final Image img_boom = boom.getImage();
        final Image img_fumaca = fumaca.getImage();
        for(int i=0; i<altura; i++){
            for(int j=0; j<largura; j++){
                if(matrix[i][j]==ATIRANDO){
                    g.drawImage(img_agua, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1, null);
                    g.drawImage(img_boom, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1, null);
                }
                else{
                    switch(tabuleiro.getTabuleiro(i, j)){
                        case Tabuleiro.AGUA:
                        case Tabuleiro.BARCO:
                            g.drawImage(img_agua, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1, null);
                            break;
                        case Tabuleiro.TIRO_AGUA:
                            g.drawImage(img_agua, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1, null);
                            g.drawImage(img_fumaca, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1, null);
                            break;
                        case Tabuleiro.TIRO_BARCO:
                            g.drawImage(img_agua, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1, null);
                            g.drawImage(img_fogo, TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1, null);
                            break;
                        default:
                    }
                }
                //g.fillRect(TAVULEIRO_INICIO_X+TAVULEIRO_TAMANHO_CASA*i+1, TAVULEIRO_INICIO_Y+TAVULEIRO_TAMANHO_CASA*j+1, TAVULEIRO_TAMANHO_CASA-1, TAVULEIRO_TAMANHO_CASA-1);
            }
        }
    }
    public void Atirar(int i, int j){
        matrix[i][j]=ATIRANDO;
    }
    private void atualizaAnimacoes(){
        if(animationCounterAgua>=6){
            animationCounterAgua=1;
        }
        else{ 
            animationCounterAgua++;
        }
        if(animationCounterFogo>=12){
            animationCounterFogo=1;
        }
        else{ 
            animationCounterFogo++;
        }
        if(animationCounterBanana>=8){
            animationCounterBanana=1;
        }
        else{ 
            animationCounterBanana++;
        }
        if(animationCounterBoom>=17){
            for(int i=0; i<altura; i++){
                for(int j=0; j<largura; j++){
                    matrix[i][j]=NORMAL;
                }
            }
            animationCounterBoom=1;
        }
        else{ 
            animationCounterBoom++;
        }
        if(animationCounterFumaca>=11){
            animationCounterFumaca=1;
        }
        else{ 
            animationCounterFumaca++;
        }
    
    }
}
