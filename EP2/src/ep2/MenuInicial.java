package ep2;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class MenuInicial extends javax.swing.JFrame {

    public MenuInicial() {
        setLocationRelativeTo(null);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        inicioMapa = new javax.swing.JButton();
        inicioAleatorio = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Batalha Naval 2.0");
        setLocation(new java.awt.Point(0, 0));

        jLabel1.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        jLabel1.setText("Batalha Naval 2.0");

        inicioMapa.setFont(new java.awt.Font("MV Boli", 0, 18)); // NOI18N
        inicioMapa.setText("Inicio com mapa(Arquivo txt)");
        inicioMapa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inicioMapaActionPerformed(evt);
            }
        });

        inicioAleatorio.setFont(new java.awt.Font("MV Boli", 0, 18)); // NOI18N
        inicioAleatorio.setText("Inicio aleatório");
        inicioAleatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inicioAleatorioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(inicioMapa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(inicioAleatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(inicioMapa, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(inicioAleatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 44, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void inicioMapaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inicioMapaActionPerformed
        int returnVal = fileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
           File file = fileChooser.getSelectedFile();
            try {
                Leitura arq = new Leitura(file);
                int altura=arq.getAltura();
                int largura=arq.getLargura();
                Tabuleiro tabuleiro = new Tabuleiro(altura , largura);
                for(int i=0; i<altura; i++){
                    for(int j=0; j<largura; j++){
                        tabuleiro.setTabuleiro(i, j, arq.getMatrix(i, j));
                    }
                }
                JogoJanela jogoJanela = new JogoJanela(tabuleiro, altura*3);
            } catch (IOException ex) {
                Logger.getLogger(MenuInicial.class.getName()).log(Level.SEVERE, null, ex);
            }catch (NullPointerException | NoSuchElementException ex){
                JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo");
            }
            // JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo");
            // JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo");
            
            // JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo");
            
               // JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo");
  
        }
    }//GEN-LAST:event_inicioMapaActionPerformed

    private void inicioAleatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inicioAleatorioActionPerformed
        Tabuleiro tabuleiro = new Tabuleiro(10,10);
        tabuleiro.PosicionarBarcos();
        new JogoJanela(tabuleiro, 30);
        
        
    }//GEN-LAST:event_inicioAleatorioActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new MenuInicial().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JButton inicioAleatorio;
    private javax.swing.JButton inicioMapa;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
